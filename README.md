Hi.

Here is materials from NXP seminar with practical part.

You can find workflow in handson_instructions_en_full_nfc_rpi_plus.pdf
This guide will show you how to access
common peripherals and interfaces of ARM-based systems-on-chip (SoC) running
Embedded Linux.

Hardware:
- VisionCB-STD base board
- VisionSOM6-ULL based on i.MX6ULL SoC
- NXP FXAS2100x gyroscope
- NTAG-I2C Plus programmable NFC tag
- PN7150 NFC expansion board
- uSD card
- UTP cable
- micro USB cable

You can use Windows or Linux as Host OS for uSD card manipulation and serial debug console.
Also it would be helpful if you've got an Android phone with NFC.